import re

from os import fsdecode
from os.path import join, isfile, normpath
from win32com.client import Dispatch


def check_zbrush_exe(path=str):
    """
    A simple function to check the zbrush path
    :param path: Give the absolute Zbrush path
    :return: Boolean if the path is ok
    """
    zb_exe = normpath(join(fsdecode(path), 'ZBrush.exe'))
    try:
        if isfile(zb_exe):
            return True

    except FileNotFoundError:
        return False


def check_zbrush_version(path=str):
    """
    Function to find the main version
    :param path: Absolute path to Zbrush
    :return: Zbrush version
    """
    zb_exe = normpath(join(fsdecode(path), 'ZBrush.exe'))
    try:
        parser = Dispatch("Scripting.FileSystemObject")
        version = parser.GetFileVersion(zb_exe)
        version = re.search(r'\d+', version).group()

        return int(version)

    except TypeError:
        return False
