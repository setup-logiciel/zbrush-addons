import win32com.client
import win32api
import win32gui
import os

from os.path import normpath

# Main
timer_on = 1  # set the inf loop variable

# create shell script
shell = win32com.client.Dispatch("WScript.Shell")

# create command to fire up Z
zbrush = normpath(r'C:\Program Files\Pixologic\ZBrush 2020\ZBrush.exe')

# os.startfile(command) #fire up Zgg

# pause for z to load
win32api.Sleep(5000)

# get title window of z
winName = win32gui.GetWindowText(win32gui.GetForegroundWindow())

# save title window of z
zWin = winName

# pause for z to be active
win32api.Sleep(5000)

# make sure z is active app
shell.AppActivate("zWin")

while(timer_on > 0):
    # sleep for 5 minutes before sending key command
    win32api.Sleep(5000)

    # making Z active app
    shell.AppActivate("zWin")
    win32api.Sleep(1000)

    if (zWin == winName):
        shell.SendKeys("w")  # initiate move command in Z
        print ("Sending Z Command")  # print statement for error check