# Zbrush-Setup
Preset, tool, addons and other middleware for Zbrush.

Try and check only on Windows 10.
This installer work with **Python 3.7** :

Zbrush Version :

| Zbrush Version |        Etat        |
|---------------:|:------------------:|
|  Zbrush 2018 : |      Non Testé     |
|  Zbrush 2019 : | :white_check_mark: |
|  Zbrush 2020 : | :white_check_mark: |


### Icon Quick Save
Files under creative commons, created by [Raindropmemory](https://www.deviantart.com/raindropmemory).
Available on [Icon Finder](https://www.iconfinder.com/icons/99421/zbrush_icon).


## Addons and Tools
### Tileable Tools
Outils zbrush pour la création de texture tileable.
Installation automatisé : :white_check_mark:
Credit : [Lukas Patrus](https://www.artstation.com/artwork/ndrPo)

### Quick Save
Installation automatisé : :white_check_mark:
Chargement automatique du *last save* de zbrush et création d'un raccourcis sur le bureau utilisateur.


### Nicks Tools
Installation automatisé : :white_check_mark:
Serie de scripts pour automatisé diverses étapes. |Plus d'information sur le Artstation de Nick Miller](https
://artofnickmiller.artstation.com/zplugin-nickstools).

## Tools
Cette section regroupe des outils pour Zbrush
- Zbrush.xml est un *langage utilisateur* pour notepad ++


## Ui
Installation de mon interface Zbrush.