::Unofficial ZBrush QuickSave Loader for Windows 
::v1.0 6/6/2016
::By Joseph Drust

::This Bat file will look for the last updated quicksave in the directory launch ZBrush and open the file
::*** Make sure that ZBrush is set to as the default program for *.zpr files ***
::This program has only been tested in a few machines so it may not work 100% of the time

::To Install

:: 1) Copy QuickSave_LoadLast.bat file to C:\Users\Public\Documents\ZBrushData\QuickSave\
:: 2) Create a Shortcut to the QuickSave_LoadLast.bat file
:: 3) Move Shortcut to Desktop
:: 4) Double Click the Shortcut to launch ZBrush and load the last modified Quicksave

:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

::To Install a Shortcut that can be launched from the Taskbar in Windows

:: 1) Locate CMD.exe on your computer
:: 2) Create a Shortcut to CMD.exe (it should generate on your desktop.)
:: 3) Right Click on the Shortcut and select Properties
:: 4) Under the 'Target' area ***ADD*** (Make sure to ***ADD*** to the exsisting target (not replace) and that there is a space before the /C):
::  /C C:\Users\Public\Documents\ZBrushData\QuickSave\QuickSave_LoadLast.bat

:: example target: C:\Windows\SysWOW64\cmd.exe /C C:\Users\Public\Documents\ZBrushData\QuickSave\QuickSave_LoadLast.bat

:: 5) Under 'Start in' area change the target to:
::  C:\Users\Public\Documents\ZBrushData\QuickSave\

:: 6) Click Apply and Ok
:: 7) Drag or Right Click 'Pin to Taskbar' on the Shortcut
:: 8) Clicking on the shortcut from the Taskbar should launch ZBrush and load the last quicksave
:: 9) You can change the icon on the Shortcut if needed :)


:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
 

for /f "eol=: delims=" %%F in ('dir /b /od *.zpr') do @set "newest=%%F"

Start "" "%newest%"