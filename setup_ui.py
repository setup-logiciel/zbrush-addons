from os.path import join, dirname, abspath, isfile, normpath
from setup_notification import info_install
from shutil import copy


def load_ui(version):
    zb_data = r"C:\Users\Public\Documents\ZBrushData{0}\ZStartup".format(version)
    config_file = join(dirname(abspath(__file__)), r'Ui\CustomUserInterface{0}.cfg'.format(version))
    msg = 'Zbrush Ui install {0}'.format(version)

    try:
        f = open(normpath(config_file))
        f.close()
        if isfile(normpath(config_file)):
            copy(src=config_file, dst=zb_data)
            print('Ui file {0} is available'.format(version))
            msg = msg + ' success.'
            info_install('Zbrush Ui', msg)
            return print('Success Load Ui')

    except FileNotFoundError:
        msg = msg + ' failed.'
        info_install('Zbrush Ui', msg)
        return print(msg)
