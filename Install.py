from os.path import normpath

from setup_check import check_zbrush_exe, check_zbrush_version
from setup_addons import install_quick_save, install_tileable_tools, install_stilobique_batch, install_nick_tools
from setup_ui import load_ui
from setup_notification import info_install
from setup_brushes import link_brush

print('Give an absolute path like "{0}" : '.format(normpath('C:\\Program Files\\Pixologic\\ZBrush\\')))
zb_path = input()
if check_zbrush_exe(zb_path):
    print('Your Zbrush path is : {0}'.format(zb_path))
    zb_version = check_zbrush_version(zb_path)
    zb_path_data = 'C:\\Users\\Public\\Documents\\ZBrushData{0}\\'.format(zb_version)

    # Install Addons
    ad_quick_save = install_quick_save(zb_path, zb_path_data)
    ad_tile_tools = install_tileable_tools(zb_path)
    ad_batch_stilobique = install_stilobique_batch(zb_path)
    ad_nick_tools = install_nick_tools(zb_path)

    ad_dic = {
        'Quick Save': ad_quick_save,
        'Tileable Tools': ad_tile_tools,
        'Batch Panel': ad_batch_stilobique,
        'Nicks Tools': ad_nick_tools
    }
    msg = ''
    i = 0
    for name, value in ad_dic.items():
        msg = "{0}\n {1} : {2}".format(msg, name, value)
        i += 1

    info_install('Zbrush Addons', msg)

    # Installation Ui
    load_ui(zb_version)

    # Install brushes
    link_brush(zb_path)

else:
    print('Wrong Zbrush Path !')
