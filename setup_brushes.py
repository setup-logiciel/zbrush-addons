from os import symlink
from os.path import join, exists
from setup_notification import info_install


def link_brush(zb_path):
    title_notification = 'Zbrush Brushes'
    folder_link = join(zb_path, 'ZBrushes')
    folder_resources = r"W:\Resources\Zbrush\Brushs"

    try:
        exists(folder_link)
        msg = 'Add brushes folders success'
        symlink(folder_resources, join(folder_link, 'Perso'))
        info_install(title_notification, msg)
        return print('Success Load Ui')

    except FileNotFoundError:
        msg = 'Install brushes failed.'
        info_install(title_notification, msg)
        return print(msg)

    except FileExistsError:
        msg = 'Folders already exist'
        info_install(title_notification, msg)
        return print(msg)
