import winshell

from os import walk, makedirs
from os.path import join, dirname, abspath, relpath, normpath, exists
from shutil import copy
from win32com.client import Dispatch

from setup_check import check_zbrush_version


def install_quick_save(zb_path, zb_data_path):
	"""
	Simple function to copy the bat on brush data and create a shortcut to use it
	:param zb_path: Absolute path to Zbrush
	:param zb_data_path: Absolute path to brush Data
	:return: Give a boolean about the success
	"""
	zb_data = join(zb_data_path, 'QuickSave')
	bat_file = join(dirname(abspath(__file__)), r'Addons\Quick Save\QuickSave_LoadLast.bat')
	zb_version = check_zbrush_version(zb_path)

	try:
		copy(src=bat_file, dst=zb_data)
		shortcut_quick_save(zb_version)
		return 'Success'
	except TypeError or FileNotFoundError:
		return 'Failed'


def shortcut_quick_save(zb_version):
	"""
	Function to create a windows shortcut
	:param zb_version: Integer information
	:return: Take the Zbrush lnk path
	"""
	windows_folder = winshell.programs()
	path = join(windows_folder, "Zbrush {0} load last Save.lnk".format(zb_version))
	path_data = normpath(r'C:\Users\Public\Documents\ZBrushData{0}'.format(zb_version))
	target = r"C:\WINDOWS\system32\cmd.exe"
	argument = r" /C {0}\QuickSave\QuickSave_LoadLast.bat".format(path_data)
	working_directory = join(path_data, 'QuickSave')
	icon = join(dirname(abspath(__file__)), 'Ui', 'IconQuickSave.ico')

	shell = Dispatch('WScript.Shell')
	shortcut = shell.CreateShortCut(path)
	shortcut.TargetPath = target
	shortcut.Arguments = argument
	shortcut.WorkingDirectory = working_directory
	shortcut.IconLocation = icon
	shortcut.save()

	return path


def install_tileable_tools(zb_path=str):
	"""
	Function to install the Add-on *Tileable Tools* on Zbrush
	:param zb_path: Absolute path to Zbrush, string type
	:return: String state information
	"""
	tileable_path = join(dirname(abspath(__file__)), r'Addons\Tileable Tools\ZBrush Install')
	try:
		loop_file(tileable_path, zb_path)
		return 'Success'

	except TypeError:
		return 'Failed'


def install_stilobique_batch(zb_path):
	"""
	Function to install the Add-on *Stilobique Batch* on Zbrush
	:param zb_path: Absolute path to Zbrush
	:return: String state information
	"""
	batch_path = join(dirname(abspath(__file__)), r'Addons\Batch\Plugins')
	zb_path = join(zb_path, 'Zstartup', 'ZPlugs64')
	try:
		loop_file(batch_path, zb_path)
		return 'Success'

	except TypeError:
		return 'Failed'

	except FileNotFoundError:
		return 'Failed, file not found'

	except PermissionError:
		return 'Wrong permission file'


def install_nick_tools(zb_path):
	"""
	Function to install Nicks Tools on Zbrush install folders.
	:param zb_path: path about the zbrush exe.
	:return: String state information
	"""
	folder_plugin = join(dirname(abspath(__file__)), r'Addons\Nicks Tools')
	zb_path = join(zb_path, 'Zstartup', 'ZPlugs64')

	try:
		loop_file(folder_plugin, zb_path)
		return 'Success'
	except TypeError:
		return 'Failed'
	except FileNotFoundError:
		return 'Failed, file not found'
	except PermissionError:
		return 'Wrong permission file'


def loop_file(path, zb_path):
	for root, dirs, files in walk(path):
		for name in files:
			src = join(root, name)
			path_dst = join(zb_path, relpath(src, path))
			if not exists(dirname(path_dst)):
				makedirs(dirname(path_dst))
			copy(src=src, dst=path_dst)
